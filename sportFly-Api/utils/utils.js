const jsonwebtoken = require("jsonwebtoken");
const fs = require("fs");
const path = require("path");
const groups = require("../crud/groups");
const roles = require("../crud/roles");

const util = require("util");

const signProm = util.promisify(jsonwebtoken.sign);
const verifyProm = util.promisify(jsonwebtoken.verify);

const pathToKey = path.join(__dirname, "/cryptography", "id_rsa_priv.pem");
const pathToPubKey = path.join(__dirname, "/cryptography", "id_rsa_pub.pem");

const PRIV_KEY = fs.readFileSync(pathToKey, "utf8");
const PUB_KEY = fs.readFileSync(pathToPubKey, "utf8");

/**
 * @param {*} user - The user object.  We need this to set the JWT `sub` payload property to the MongoDB user ID
 */
async function issueJWT(user) {
  const expiresIn = "1d";
  const actions = await buildPermissions(user.group, (error, act) => {
    if (error) console.log(error);
  });
  // console.log(`Estas son las acciones que puede efectuar el user ${user.email}  :${actions}`)
  const payload = {
    sub: user.sub,
    iat: Date.now(),
    actions: actions,
    email: user.email,
  };

  const signedToken = await signProm(
    payload,
    { key: PRIV_KEY, passphrase: "top secret" },
    { expiresIn: expiresIn, algorithm: "RS256" }
  );

  return {
    token: "Bearer " + (await signedToken),
    expires: expiresIn,
  };
}

function authMiddleware(req, res, next) {
  const tokenParts = req.headers.authorization.split(" ");

  if (
    tokenParts[0] === "Bearer" &&
    tokenParts[1].match(/\S+\.\S+\.\S+/) !== null
  ) {
    try {
      const verification = jsonwebtoken.verify(tokenParts[1], PUB_KEY, {
        algorithms: ["RS256"],
      });
      req.jwt = verification;
      next();
    } catch (err) {
      res
        .status(401)
        .json({
          success: false,
          msg: "You are not authorized to visit this route",
        });
    }
  } else {
    res
      .status(401)
      .json({
        success: false,
        msg: "You are not authorized to visit this route",
      });
  }
}

/**
 * @param {JSON} token - A Object json with Attribute of a response 0auth
 *
 * @param {(Error,JSON)=>{}} done -A Error return if not complete .save in otherwise Error===null and JSON response
 *
 * @return {Promise<JSON>} The response
 *
 */

async function verifyToken(token, done) {
  try {
    const tokenParts = token.split(" ");
    if (
      tokenParts[0] === "Bearer" &&
      tokenParts[1].match(/\S+\.\S+\.\S+/) !== null
    ) {
      const verification = await verifyProm(tokenParts[1], PUB_KEY, {
        algorithms: ["RS256"],
      });
      done(verification, null);
      return verification;
    } else {
      done(new Error("You are not authorized to visit this route"), null);
      return;
    }
  } catch (err) {
    console.log(err);
    done(err, null);
  }
}

/**
 * @param {JSON} idGroups - idGroups
 *
 * @param {(Error,[])=>{}} done -A Error return if not complete  in otherwise Error===null and JSON list permission is return
 *
 * @return {Promise<[]>} A array with all permissions
 *
 */
async function buildPermissions(idGroups, done) {
  if (!idGroups) {
    done(null, []);
    return [];
  }
  const docGroup = await groups.getGroup(idGroups, (error, doc) => {
    if (error) done(error, []);
    //console.log(`Este es el group con id ${idGroups} cuyo contenido es ,${doc}`);
  });
  if (!docGroup) return [];
  //const docRoll= await roles.getRoll(docGroup.roll)
  const permiss = await docGroup.roll.reduce(
    async (accPermissPrevius, itRoll) => {
      let accPermiss = await accPermissPrevius;
      const docRol = await roles.getRoll(itRoll, (error, doc_rol) => {
        if (error) done(error, null);
      });
      //console.log(`Para el id ${itRoll} esta es la lista de roles ${docRol}`)
      return accPermiss.concat(
        docRol.actions.map((itAct) => Object.keys(itAct).join())
      );
    },
    []
  );
  done(null, permiss);
  return permiss;
}

module.exports.issueJWT = issueJWT;
module.exports.authMiddleware = authMiddleware;
module.exports.verifyToken = verifyToken;
module.exports.buildPermissions = buildPermissions;
