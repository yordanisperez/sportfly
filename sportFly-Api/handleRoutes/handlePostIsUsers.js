const User = require("../crud/users");
const utils = require("../utils/utils");
const newUserRequest = require("../crud/requestNewUser");

const TIMEOUT = 10000;
/*=======================================================
      =Handle new User:Res send obj json                    = 
      =In body receive email, sub and name                  =
      =response obj json {                                  =
      =  success: true,                                     =
      =  token: tokenObject.token,                          =
      =  expiresIn: tokenObject                             =
      =  }                                                  =
      =tokenObject:{                                        =
      =  token: "Bearer " + signedToken,                    =
      =  expires: expiresIn                                 =
      = }                                                   =
      ======================================================*/
async function handlePostIsUsers(req, res, next) {
  //  console.log("Manipulador de petición 'handlePostIsUsers' fue llamado. ",req.body);
  if (!req.body.email || !req.body.sub || !req.body.name) {
    res.status(403).json({ success: false });
    return;
  }
  const email = req.body.email;
  const sub = req.body.sub;
  const name = req.body.name;
  try {
    let t = setTimeout(() => {
      next({ message: "timeout" });
    }, TIMEOUT);
    User.getUserForEmail(email, async (error, user) => {
      clearTimeout(t);

      if (error) {
        console.log("error: ", error);
        return next(error);
      }
      if (user) {
        //Ya existe ese usuario en la base de datos,
        //se debe construir el objeto con todos los permisos
        const tokenObject = await utils.issueJWT(user);
        res
          .status(200)
          .json({
            success: true,
            token: tokenObject,
            expiresIn: tokenObject.expires,
          });
      } else {
        //Agregamos el ussuario
        t = setTimeout(() => {
          next({ message: "timeout" });
        }, TIMEOUT);
        /*
                        Here have validate this user before add to system.
                    */

        const newRequest = await newUserRequest.createRequestNewUser(
          { name: name, sub: sub, email: email },
          async (error, user) => {
            clearTimeout(t);
            if (error) {
              return next(error);
            }
          }
        );
        res.status(200).json({ success: true, _idRequest: newRequest._id });
      }
    });
  } catch (error) {
    console.log("manejado handlePostUsers", error);
  }
}

function handleReqCreateAsociado(req, res, next) {
  res
    .status(404)
    .json({ success: false, msg: "Esta funcionalidad aun no se implementa" });
}
function handleReqCreateClub(req, res, next) {
  res
    .status(404)
    .json({ success: false, msg: "Esta funcionalidad aun no se implementa" });
}
function handleReqCreateSeguidor(req, res, next) {
  res
    .status(404)
    .json({ success: false, msg: "Esta funcionalidad aun no se implementa" });
}
exports.handlePostIsUsers = handlePostIsUsers;
exports.handleReqCreateAsociado = handleReqCreateAsociado;
exports.handleReqCreateClub = handleReqCreateClub;
exports.handleReqCreateSeguidor = handleReqCreateSeguidor;
