/**
 * @fileoverview  model Roll
 *
 * @version      0.1
 *
 * @author       Yordanis Pérez Brito <yordanis@gmail.com>
 *
 * @copyright    sportfly
 */

const { Schema, model } = require("mongoose");

/**
 * @property {String} description - The description of Roll.
 *
 * @property {String} abrev -Identify not verbose.
 *
 * @property {[String]} actions -A array with list actions.
 *
 */
const RollSchema = new Schema({
  description: {
    type: String,
    required: true,
  },
  abrev: {
    type: String,
    required: true,
  },
  actions: [{}],
});

module.exports = model("Roll", RollSchema);
