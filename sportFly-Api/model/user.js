/**
 * @fileoverview  model User
 *
 * @version      0.1
 *
 * @author       Yordanis Pérez Brito <yordanis@gmail.com>
 *
 * @copyright    sportfly
 */

const { Schema, model, Types } = require("mongoose");

/**
 * @property {String} name - The name of user.
 *
 * @property {String} email -The email of user.
 *
 * @property {String} sub -A string with id of user using for 0auth.
 *
 * @property {Types.ObjectId} group -The Group to User
 *
 */

const UserSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  sub: {
    type: String,
    required: true,
  },
  group: {
    type: Types.ObjectId,
  },
});

module.exports = model("User", UserSchema);
