/**
 * @fileoverview  model Groups
 *
 * @version      0.1
 *
 * @author       Yordanis Pérez Brito <yordanis@gmail.com>
 *
 * @copyright    sportfly
 */

const { Schema, model } = require("mongoose");

/**
 * @property {String} description - The description of Groups. Sample Groups of all asociate
 *
 * @property {String} abrev -Identify not verbose. Sample Asociado
 *
 * @property {[Schema.Types.ObjectId]} roll -A array with list roll asociate a this group
 *
 */

const GroupSchema = new Schema({
  description: {
    type: String,
    required: true,
  },
  abrev: {
    type: String,
    required: true,
  },
  roll: [Schema.Types.ObjectId],
});

module.exports = model("Group", GroupSchema);
