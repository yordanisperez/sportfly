/**
 * @fileoverview Operations crud for model Users
 *
 * @version      0.1
 *
 * @author       Yordanis Pérez Brito <yordanis@gmail.com>
 *
 * @copyright    sportfly
 */

const User = require("../model/user");

/**
 * @param {JSON} userJson - A Object json with Attribute of a User
 *
 * @param {(Error,JSON)=>{}} done -A Error return if not complete .save in otherwise Error===null and JSON is the document save
 *
 * @return {JSON} The document save
 *
 */

const createNewUser = async function (userJson, done) {
  let doc = new User(userJson);
  await doc
    .save()
    .then((saveDoc) => {
      done(null, doc);
    })
    .catch((error) => {
      done(error, doc);
    });
  return doc;
};

/**
 * @param {(Error,Array)=>{}} done -A Error return if not complete  in otherwise Error===null and Array List Document find
 *
 * @return {Array} The Array of all documents
 *
 */
const findAllUser = async (done) => {
  try {
    const doc = await User.find({}, "name").exec();
    done(doc, null);
    return doc;
  } catch (error) {
    done(error, null);
  }
};

/**
 * @param {import('mongoose').Types.ObjectId} idUser - _id of User
 *
 * @param {(Error,JSON)=>{}} done -A Error return if not complete .save in otherwise Error===null and JSON is the document search
 *
 * @return {JSON} The document search
 *
 */
const getUser = async (idUser, done) => {
  try {
    const doc = await User.findOne({ _id: idUser }).exec();
    done(null, doc);
    return doc;
  } catch (error) {
    done(error, null);
  }
};

/**
 * @param {String} name - Name of User for search
 *
 * @param {(Error,JSON)=>{}} done -A Error return if not complete .save in otherwise Error===null and JSON is the document search
 *
 * @return {JSON} The document search
 *
 */
const getUserForName = async (name, done) => {
  try {
    const doc = await User.findOne({ name: name }).exec();
    done(doc, null);
    return doc;
  } catch (error) {
    done(error, null);
  }
};

/**
 * @param {String} email - E-mail of User for search
 *
 * @param {(Error,JSON)=>{}} done -A Error return if not complete .save in otherwise Error===null and JSON is the document search
 *
 * @return {JSON} The document search
 *
 */
const getUserForEmail = async (email, done) => {
  try {
    const doc = await User.findOne({ email: email }).exec();
    done(null, doc);
    return doc;
  } catch (error) {
    done(error, null);
  }
};

/**
 * @param {(Error,Object)=>{}} done -A Error return if not complete  in otherwise Error===null and a Object with count document delete
 *
 * @return {Object} Object with count document delete
 *
 */
const removeAllUser = async (done) => {
  try {
    const docs = await User.deleteMany().exec();
    done(null, docs);
  } catch (error) {
    done(error, null);
  }
};

exports.createNewUser = createNewUser;
exports.findAllUser = findAllUser;
exports.getUser = getUser;
exports.getUserForName = getUserForName;
exports.getUserForEmail = getUserForEmail;
exports.removeAllUser = removeAllUser;
