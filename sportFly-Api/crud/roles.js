/**
 * @fileoverview Operations crud for model Roll
 *
 * @version      0.1
 *
 * @author       Yordanis Pérez Brito <yordanis@gmail.com>
 *
 * @copyright    sportfly
 */

const Roll = require("../model/roles");

/**
 * @param {JSON} rollJson - A Object json with Attribute of a Roll
 *
 * @param {(Error,JSON)=>{}} done -A Error return if not complete .save in otherwise Error===null and JSON is the document save
 *
 * @return {JSON} The document save
 *
 */
const createNewRoll = async function (rollJson, done) {
  let doc = new Roll(rollJson);
  await doc
    .save()
    .then((saveDoc) => {
      done(null, doc);
    })
    .catch((error) => {
      done(error, doc);
    });
  return doc;
};

/**
 * @param {(Error,Array)=>{}} done -A Error return if not complete  in otherwise Error===null and Array List Document find
 *
 * @return {Array} The Array of all documents
 *
 */
const findAllRoll = async (done) => {
  try {
    const doc = await Roll.find({}).exec();
    done(doc, null);
    return doc;
  } catch (error) {
    done(error, null);
  }
};

/**
 * @param {import('mongoose').Types.ObjectId} idRoll - _id of Roll
 *
 * @param {(Error,JSON)=>{}} done -A Error return if not complete .save in otherwise Error===null and JSON is the document save
 *
 * @return {Promise<JSON>} -The document save
 *
 */
const getRoll = async (idRoll, done) => {
  try {
    const doc = await Roll.findOne({ _id: idRoll }).exec();
    done(null, doc);
    return doc;
  } catch (error) {
    console.log(error);
    done(error, null);
  }
};

/**
 * @param {(Error,Object)=>{}} done -A Error return if not complete  in otherwise Error===null and a Object with count document delete
 *
 * @return {Object} Object with count document delete
 *
 */
const removeAllRoll = async (done) => {
  try {
    const docs = await Roll.deleteMany().exec();
    done(null, docs);
  } catch (error) {
    done(error, null);
  }
};

exports.createNewRoll = createNewRoll;
exports.findAllRoll = findAllRoll;
exports.getRoll = getRoll;
exports.removeAllRoll = removeAllRoll;
