/**
 * @fileoverview Operations crud for model Groups
 *
 * @version      0.1
 *
 * @author       Yordanis Pérez Brito <yordanis@gmail.com>
 *
 * @copyright    sportfly
 */

const Group = require("../model/groups");

/**
 * @param {JSON} groupJson - A Object json with Attribute of a Group
 *
 * @param {(Error,JSON)=>{}} done -A Error return if not complete .save in otherwise Error===null and JSON is the document save
 *
 * @return {JSON} The document save
 *
 */
const createNewGroup = async function (groupJson, done) {
  let doc = new Group(groupJson);
  await doc
    .save()
    .then((saveDoc) => {
      done(null, doc);
    })
    .catch((error) => {
      done(error, doc);
    });
  return doc;
};
/**
 * @param {(Error,Array)=>{}} done -A Error return if not complete  in otherwise Error===null and Array List Document find
 *
 * @return {Array} The Array of all documents
 *
 */
const findAllGroups = async (done) => {
  try {
    const doc = await Group.find({}).exec();
    done(doc, null);
    return doc;
  } catch (error) {
    done(error, null);
  }
};
/**
 * @param {import('mongoose').Types.ObjectId} idGroup - _id of group
 *
 * @param {(Error,Document)=>{}} done -A Error return if not complete in otherwise Error===null and  the document group search
 *
 * @return {Promise<import('mongoose').Document>} the document group search
 *
 */
const getGroup = async (idGroup, done) => {
  try {
    const doc = await Group.findOne({ _id: idGroup }).exec();
    done(null, doc);
    return doc;
  } catch (error) {
    done(error, null);
  }
};
/**
 * @param {(Error,Object)=>{}} done -A Error return if not complete  in otherwise Error===null and a Object with count document delete
 *
 * @return {Object} Object with count document delete
 *
 */
const removeAllGroups = async (done) => {
  try {
    const docs = await Group.deleteMany().exec();
    done(null, docs);
  } catch (error) {
    done(error, null);
  }
};

exports.createNewGroup = createNewGroup;
exports.findAllGroups = findAllGroups;
exports.getGroup = getGroup;
exports.removeAllGroups = removeAllGroups;
