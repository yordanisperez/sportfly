/**
 * @fileoverview Operations crud for model RequestNewUser
 *
 * @version      0.1
 *
 * @author       Yordanis Pérez Brito <yordanis@gmail.com>
 *
 * @copyright    sportfly
 */

const RequestNewUser = require("../model/requestNewUser");

/**
 * @param {JSON} userJson - A Object json with Attribute of a User
 *
 * @param {(Error,JSON)=>{}} done -A Error return if not complete .save in otherwise Error===null and JSON is the document save
 *
 * @return {Promese<JSON>} The document save
 *
 */

const createRequestNewUser = async function (userJson, done) {
  let doc = new RequestNewUser(userJson);
  await doc
    .save()
    .then((saveDoc) => {
      done(null, doc);
    })
    .catch((error) => {
      done(error, doc);
    });
  return doc;
};

/**
 * @param {import('mongoose').Types.ObjectId} idUserRequest - _id of User
 *
 * @param {(Error,JSON)=>{}} done -A Error return if not complete .save in otherwise Error===null and JSON is the document search
 *
 * @return {Promese<JSON>} The document search
 *
 */
const getUserRequest = async (idUserRequest, done) => {
  try {
    const doc = await RequestNewUser.findOne({ _id: idUserRequest }).exec();
    done(null, doc);
    return doc;
  } catch (error) {
    done(error, null);
  }
};

/**
 * @param {(Error,Object)=>{}} done -A Error return if not complete  in otherwise Error===null and a Object with count document delete
 *
 * @return {Object} Object with count document delete
 *
 */
const removeAllRequestUser = async (done) => {
  try {
    const docs = await User.deleteMany().exec();
    done(null, docs);
  } catch (error) {
    done(error, null);
  }
};

exports.createRequestNewUser = createRequestNewUser;
exports.getUserRequest = getUserRequest;
exports.removeAllRequestUser = removeAllRequestUser;
