require("dotenv").config();
const express = require("express");
const app = express();
const cors = require("cors");
const db = require("./conection/conectDb");
const path = require("path");
const enviroment_prod = require("./enviroment-prod");

const { NODE_ENV, URL_ALLOW, PORT } = process.env;

var corsOptions = {
  origin: URL_ALLOW,
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

const port = PORT || 8080;

app.use(cors(corsOptions));
app.use(express.urlencoded({ extended: true })); //Usar el bodyparser que enta

app.use(express.static(path.join(__dirname + "/../client/build")));

if (NODE_ENV !== "test") enviroment_prod(app, db);

const server = app.listen(port, function () {
  console.log(`Listening on port ${port}`);
});

module.exports = { app, server, db };
