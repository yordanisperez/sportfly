const mongoose = require("mongoose");

const dataRoles = [
  {
    _id: mongoose.Types.ObjectId("000000000000000000000000"),
    description:
      "EL roll asociado le permite ser incluido en un club existente y participar en sus competiciones deportivas",
    actions: [
      { create_club: "Puede o no Crear Un Club" },
      { register_pigeon: "Registrar sus palomas" },
      {
        register_pigeon_fly:
          "Registrar sus palomas en cada competición que participara",
      },
      {
        register_house:
          "Registrar la ubicación de su palomar utilizando el componente de mapas diseñado para tal fin",
      },
      {
        send_data:
          "Enviar los datos de las palomas al terminar cada competición",
      },
    ],
    abrev: "Asociado",
  },
  {
    _id: mongoose.Types.ObjectId("000000000000000000000001"),
    description: "Administrar una Sociedad o Club",
    actions: [
      { add_member: "Adicionar nuevos asociados al CLUB" },
      { set_plan: "Establecer el plan de competencias anual para el CLUB" },
      {
        set_point_fly:
          "Establecer cada punto de suelta de la temporada utilizando el componente de mapas diseñado para tal fin",
      },
      {
        certify_house:
          "Certificar la ubicación del palomar de cada asociado que competirá",
      },
      { set_hours: "Establecer la hora de suelta de cada competición" },
      { certify_pigeon: "Certifica cada paloma encestada" },
      {
        certify_sport:
          "Al final de la competencia, el administrador del club certifica",
      },
      { finish_sport: "Da por terminada la competición" },
      {
        timing_method:
          "Seleccionará el método de cronometraje que se ajuste a su competición",
      },
    ],
    abrev: "Administrador",
  },

  {
    _id: mongoose.Types.ObjectId("000000000000000000000002"),
    description: "Sigue a un miembro",
    actions: [
      { follow_member: "Seguir a su competidor favorito" },
      {
        decisions_member:
          "Estará al tanto de todas sus decisiones en el momento que estas sean tomadas",
      },
      {
        info_member:
          "Recibirá toda la información de las competencias en las que este participe",
      },
    ],
    abrev: "Seguidor",
  },
];
const dataUsers = [
  {
    _id: mongoose.Types.ObjectId("000000000000000000000000"),
    name: "yordanis",
    email: "yordanis@gmail.com",
    sub: "0",
    group: mongoose.Types.ObjectId("000000000000000000000005"),
  },
  {
    _id: mongoose.Types.ObjectId("000000000000000000000001"),
    name: "pedro",
    email: "pedro@hotmail.com",
    sub: "0",
    group: mongoose.Types.ObjectId("000000000000000000000006"),
  },
];

const dataGroups = [
  {
    _id: mongoose.Types.ObjectId("000000000000000000000005"),
    description: "Asociados",
    abrev: "Asociados",
    roll: [mongoose.Types.ObjectId("000000000000000000000000")],
  },

  {
    _id: mongoose.Types.ObjectId("000000000000000000000006"),
    description: "Administradores",
    abrev: "Administradores",
    roll: [mongoose.Types.ObjectId("000000000000000000000001")],
  },
  {
    _id: mongoose.Types.ObjectId("000000000000000000000007"),
    description: "Seguidores",
    abrev: "Seguidores",
    roll: [mongoose.Types.ObjectId("000000000000000000000002")],
  },

  {
    _id: mongoose.Types.ObjectId("000000000000000000000008"),
    description: "Administrator y Asociado",
    abrev: "AdminYasociado",
    roll: [
      mongoose.Types.ObjectId("000000000000000000000000"),
      mongoose.Types.ObjectId("000000000000000000000001"),
    ],
  },
];

module.exports = { dataRoles, dataGroups, dataUsers };
