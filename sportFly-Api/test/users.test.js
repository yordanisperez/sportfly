const supertest = require("supertest");
const { app, server, db } = require("../server");
const routes = require("../routes/routes");
const failtRoutes = require("../routes/failtRoutes");
const User = require("../crud/users");
const Group = require("../crud/groups");
const Roll = require("../crud/roles");
const mongoose = require("mongoose");
const { dataGroups, dataRoles, dataUsers } = require("./testingUsers");

const utils = require("../utils/utils");

const { MONGO_HOST } = process.env;
const URITEST = `mongodb://${MONGO_HOST}/sportFlyTest?readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false`;

const api = supertest(app);

beforeAll(async () => {
  await db.connect(URITEST, (error, mongoose) => {
    if (error) {
      // se responde a cualquier ruta valida con un mensaje de servidor fuera de servicio
      console.log(error);
      app.use(failtRoutes({}));
    } else {
      const dbm = mongoose.connection;
      dbm.once("open", (_) => {
        console.log("conected: ", strHost);
      });

      dbm.on("error", (err) => {
        console.log("Ha ocurrido un error");
      });
      app.use(routes({}));
    }
  });
});
// For each testing
beforeEach(async () => {
  /* await dataRoles.map(async (itroles)=>{
       await Roll.createNewRoll(itroles,(error,data)=>{
            if (error)
              console.log(error);
        })
     })
     await dataUsers.map(async (itUser)=>{
        await User.createNewUser(itUser,(error,data)=>{
            if (error)
            console.log(error);            
         })
     })
     await dataGroups.map(async (itGroup)=>{
        await Group.createNewGroup(itGroup,(error,data)=>{
            if (error)
              console.log(error);             
         })
     })*/
  for (let i = 0; i < dataRoles.length; i++) {
    await Roll.createNewRoll(dataRoles[i], (err, dat) => {
      if (err) console.log(err);
    });
  }
  for (let i = 0; i < dataUsers.length; i++) {
    await User.createNewUser(dataUsers[i], (err, dat) => {
      if (err) console.log(err);
    });
  }
  for (let i = 0; i < dataGroups.length; i++) {
    await Group.createNewGroup(dataGroups[i], (err, dat) => {
      if (err) console.log(err);
    });
  }
});

// Cleans up database between each test
afterEach(async () => {
  await Roll.removeAllRoll((error, data) => {
    if (error) console.log(error);
  });
  await User.removeAllUser((error, data) => {
    if (error) console.log(error);
  });
  await Group.removeAllGroups((error, data) => {
    if (error) console.log(error);
  });
});

describe("Testing routing /api/isUsers", () => {
  test("The api isUsers return a 403 for a request without param and json { success: false }", async () => {
    await api
      .post("/api/isUsers")
      .expect(403, {
        success: false,
      })
      .expect("Content-Type", /application\/json/);
  });

  test("Send to api a post with user not is database.The api return _idRequest for activate this user with a link to email", async () => {
    await api
      .post("/api/isUsers")
      .send(`name=john&&email=lolo@gmail.com&&sub=123`)
      .expect(200)
      .expect(async function (res) {
        expect(res.body.success).toBe(true);
        expect(res.body._idRequest).toBeDefined();
      })
      .expect("Content-Type", /application\/json/);
  });

  test(`Send to api a post with user include in database and it is associate to group.The api return a WTJ with Group and name, email,  sub params`, async () => {
    await api
      .post("/api/isUsers")
      .send(`name=yordanis&&email=yordanis@gmail.com&&sub=123`)
      .expect(200)
      .expect(async function (res) {
        res.body.success = true;
        const result = await utils.verifyToken(
          res.body.token.token,
          (error, result) => {
            if (error) console.log(error);
          }
        );
        expect(result.email).toBe("yordanis@gmail.com");
        expect(result.sub).toBe("0");
        expect(result.actions).toEqual(
          expect.arrayContaining([
            "create_club",
            "register_pigeon",
            "register_pigeon_fly",
            "register_house",
            "send_data",
          ])
        );
      })
      .expect("Content-Type", /application\/json/);
  });
});

describe("Testing the function Utils, buildPermissions", () => {
  test("This function return array with keys actions roles when in arg receive id of groups", async () => {
    //expect.hasAssertions()
    const resultGroup5 = await utils.buildPermissions(
      "000000000000000000000005",
      (error, data) => {
        if (error) console.log(error);
      }
    );
    expect(resultGroup5).toEqual(
      expect.arrayContaining([
        "create_club",
        "register_pigeon",
        "register_pigeon_fly",
        "register_house",
        "send_data",
      ])
    );

    const resultGroup6 = await utils.buildPermissions(
      "000000000000000000000006",
      (error, data) => {
        if (error) console.log(error);
      }
    );
    expect(resultGroup6).toEqual(
      expect.arrayContaining([
        "add_member",
        "set_plan",
        "set_point_fly",
        "certify_house",
        "set_hours",
        "certify_pigeon",
        "certify_sport",
        "finish_sport",
        "timing_method",
      ])
    );

    const resultGroup8 = await utils.buildPermissions(
      "000000000000000000000008",
      (error, data) => {
        if (error) console.log(error);
      }
    );
    expect(resultGroup8).toEqual(
      expect.arrayContaining([
        "create_club",
        "register_pigeon",
        "register_pigeon_fly",
        "register_house",
        "send_data",
        "add_member",
        "set_plan",
        "set_point_fly",
        "certify_house",
        "set_hours",
        "certify_pigeon",
        "certify_sport",
        "finish_sport",
        "timing_method",
      ])
    );
  });

  test("This function return array [] empty if user the group of user is undefined", async () => {
    //expect.hasAssertions()
    const resultGroupUndefined = await utils.buildPermissions(
      undefined,
      (error, data) => {
        if (error) console.log(error);
      }
    );
    expect(resultGroupUndefined).toEqual(expect.arrayContaining([]));
  });
  test("This function return array [] empty if user the group of user not exist in database", async () => {
    //expect.hasAssertions()
    const resultGroupNotExist = await utils.buildPermissions(
      "000000000000000000000001",
      (error, data) => {
        if (error) console.log(error);
      }
    );
    expect(resultGroupNotExist).toEqual(expect.arrayContaining([]));
  });
});

afterAll(async () => {
  await db.close();
  server.close();
});
