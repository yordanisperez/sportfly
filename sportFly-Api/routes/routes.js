const express = require("express");
const handle = require("../handleRoutes/handlePostIsUsers");
const utils = require("../utils/utils");

const router = express.Router((option = {}));

module.exports = function (req, res, next) {
  // Router factory

  router.post("/api/isUsers", handle.handlePostIsUsers); //Add a new User
  router.post("/api/solicitudCreateAsociado", handle.handleReqCreateAsociado); //request, createAsociado
  router.post("/api/solicitudCreateClub", handle.handleReqCreateClub); //request, Createclub
  router.post("/api/solicitudCreateSeguidor", handle.handleReqCreateSeguidor); //request, CreateSeguidor

  return router;
};
