let mongoose;
try {
  mongoose = require("mongoose");
} catch (e) {
  console.log(e);
}
/*La función se le pasa la URI y una funcion done(error,db)=>error===null si todo va bien y db === dbmongo
 */
function connect(URI, done) {
  try {
    mongoose
      .connect(URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        // useCreateIndex: true,
        //useFindAndModify: false,
      })
      .then((db) => {
        console.log("Moongose se ha conectado :");
        done(null, mongoose);
      })
      .catch((error) => {
        console.log(
          "Ha ocurrido un error en el momento de conectar a mongo URI: ",
          URI
        );
        done(error, null);
      });
  } catch (error) {
    console.log(error);
    done(error, null);
  }
}

async function close() {
  return await mongoose.connections[0]
    .close()
    .then(() => {
      console.log("moongose se ha desconectado");
    })
    .catch((err) => {
      throw new error(
        "Ha ocurrido un error mientras se cerraban las coneciones"
      );
    });
}

module.exports = { connect, close };
