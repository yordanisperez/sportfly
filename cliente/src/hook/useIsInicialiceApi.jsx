import { useHistory } from "react-router-dom";
const serverApi = process.env.REACT_APP_API;

function useIsInicialiceApi(users) {
  const history = useHistory();

  fetch(serverApi + "/api/isUsers", {
    method: "POST",
    mode: "cors", // no-cors, *cors, same-origin
    cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
    credentials: "same-origin", // include, *same-origin, omit
    "Access-Control-Allow-Origin": serverApi,
    header: {
      "Content-Type": "application/json",
    },
    redirect: "follow", // manual, *follow, error
    referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin,  same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: new URLSearchParams(users),
  })
    .then((res) => {
      if (res.ok) {
        console.log("Ok");
        return res.json(); // Actualizamos el Contexto de la app
      }
    })
    .then((resParsed) => {
      console.log("Respuesta: ", resParsed); // <- mostramos los datos recibidos, luego de ser parseados
      history.replace("/");
    })
    .catch((error) => {
      console.log(error);
      return { error: "No se pudo comunicar con el servidor API" };
    });
}

export default useIsInicialiceApi;
